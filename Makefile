TEX := $(wildcard *.tex)
PDF := $(patsubst %.tex,%.pdf,${TEX})

pdfs : ${PDF}

arxiv.zip : full.pdf
	zip $@ full.pdf full.bbl

${PDF} : %.pdf : %.tex *.bib
	pdflatex -interaction=nonstopmode -halt-on-error $*
	bibtex $*
	pdflatex -interaction=nonstopmode -halt-on-error $*
	pdflatex -interaction=nonstopmode -halt-on-error $*

.PHONY : pdfs
